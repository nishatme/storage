---
title: "International Political Economy"
date: 2022-12-26T15:50:22+06:00
draft: false
---

Compiled from the book: **Essentials of International Relations**, *8th edition*, 2019, Karen A. Mingst et al.
Chapter: 8 (International Political Economy)

![](/irmc8-1.jpg)
![](/irmc8-2.jpg)
![](/irmc8-3.jpg)
![](/irmc8-4.jpg)
![](/irmc8-5.jpg)
![](/irmc8-6.jpg)
![](/irmc8-7.jpg)
![](/irmc8-8.jpg)
![](/irmc8-9.jpg)
![](/irmc8-10.jpg)
